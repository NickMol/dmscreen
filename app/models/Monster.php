<?php
class Monster extends Model{
	var $id;
	var $name;
	var $type;

	public function __construct(){
		parent::__construct();
	}

	public function insert(){
		$sql = "INSERT INTO Monster (name, type) VALUES (:name, :type)";
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['name'=>$this->name,'type'=>$this->type]);
	}

	/*
		for index
	*/
	public function getMonsters(){
		$sql = "SELECT * FROM Monster m LEFT JOIN Player_Monsters p ON m.id = p.monster_id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetchAll();
	}

	/*
		for encounter generator
	*/
	public function getMonsters_info($cr, $location){
		$sql = "SELECT name, challenge_rating FROM Monster, Monster_info, monster_location WHERE Monster.id = Monster_info.monster_id AND Monster_info.challenge_rating = :cr AND monster.id = monster_location.monster_id AND monster_location.location = :location ORDER BY name ASC ";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['cr'=>$cr, 'location'=>$location]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetchAll();
	}

	/*
		for stat block
	*/
	public function getMonsterStats($id){
		$sql = "SELECT * FROM Monster m, monster_info i, monster_ability a, monster_actions c, monster_location l, monster_stats s WHERE m.id =:id AND i.monster_id = :id AND a.monster_id = :id AND c.monster_id = :id AND l.monster_id = :id AND s.monster_id = :id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['id'=>$id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetchAll();
	}

	/*
		for favorite
	*/
	public function getFavorites(){
		$sql = "SELECT * FROM Monster m, Player_Monsters p WHERE p.monster_id = m.id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetchAll();
	}

	public function find($id){
		$sql = "SELECT * FROM Monster WHERE id =:id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['id'=>$id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetch();
	}

	public function search($name){
		$sql = "SELECT * FROM Monster m LEFT JOIN Player_Monsters p ON m.id = p.monster_id WHERE m.name LIKE '%" . $name ."%'";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['name'=>$name]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetchAll();
	}
	

	public function orderDesc(){
		$sql = "SELECT * FROM Monster ORDER BY type DESC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetchAll();
	}

	public function orderAsc(){
		$sql = "SELECT * FROM Monster ORDER BY type ASC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Monster");
		return $stmt->fetchAll();
	}
	
}
?>