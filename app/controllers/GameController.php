<?php
class GameController extends Controller{

	public function index(){
		$games = $this->model('Game')->getGames();
		$this->view('Game/index', ['games' => $games]);
	}

	public function goToMain($game){
		$_SESSION['game_id'] = $game;
		header('location:/Main/index');
	}

	public function create(){
		if(isset($_POST['action'])){
			$game = $this->model('Game');
			//set values to model vars 
			$game->dm_id = $_SESSION['user_id'];
			$game->game_name = $_POST['game_name'];
			$game->last_accessed = date("Y-m-d h:i:s");

			try{
				$game->insert();
				header('location:/Game/index');
			}catch(Exception $e){
				echo "in catch create" . $e;
				$this->view('Roster/create', ['errormessage' => 'Something went wrong']);
			}
		}else{
			$games = $this->model('Game')->getGames();
			$this->view('Game/index', ['games' => $games]);
		}
	}
}
?>