<?php
class Roster extends Model{
	var $id;
	var $game_id;
	var $name;
	var $armor_class;
	var $passive_perception;
	var $initiative;

	public function __construct(){
		parent::__construct();
	}

	public function insert(){
		$sql = "INSERT INTO Roster (game_id, name, armor_class, passive_perception, initiative) VALUES (:game_id, :name, :armor_class, :passive_perception, :initiative)";
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['game_id'=>$this->game_id,'name'=>$this->name,'armor_class'=>$this->armor_class,'passive_perception'=>$this->passive_perception,'initiative'=>$this->initiative]);
	}

	public function getRoster(){
		$sql = "SELECT * FROM Roster ORDER BY initiative DESC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Roster");
		return $stmt->fetchAll();
	}

	public function find($id){
		$sql = "SELECT * FROM Roster WHERE id =:id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['id'=>$id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Roster");
		return $stmt->fetch();
	}

	public function delete($game_id, $name){
		$sql = "DELETE FROM Roster WHERE game_id = :game_id AND name = :name";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['game_id'=>$game_id, 'name'=>$name]);
	}

	
	public function editInitiative($id, $initiative){
		$sql = 'UPDATE Roster SET initiative = :initiative WHERE id = :id';
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['id'=>$id,'initiative'=>$initiative]);
	}

	public function orderDesc(){
		$sql = "SELECT * FROM Roster ORDER BY initiative DESC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Roster");
		return $stmt->fetchAll();
	}
	
}
?>