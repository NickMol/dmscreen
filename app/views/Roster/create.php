<!DOCTYPE html>
<html style="height: 100%">
	<head>
		<title>DMScreen - Add a Player or Monster to Roster</title>
		<link rel="icon" href="../../images/dice.png">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		
	</head>
	<body  style="background-image: linear-gradient(rgb(255, 128, 128) , rgb(255, 204, 204)); background-repeat: no-repeat; height: 100%;">

		<div style="padding: 10%; text-align: center; min-height: 90%; align: center;">
		<form class="" action="" method="post" style="margin-right: 0px;">
		  <h3>Add a Player or Monster: </h3>
		  <img class="mb-4" src="../../images/dice.png" alt="" width="144" height="144">
		  <br>
	      <input name="name" id="inputname" class="form-control" placeholder="Name" style="width: 20%; margin: auto;">
		  <input name="AC" id="inputAC" class="form-control" placeholder="Armor Class" style="width: 20%; margin: auto;">
		  <input name="perception" id="inputPreception" class="form-control" placeholder="Passive Perception" style="width: 20%; margin: auto;">
		  <input name="initiative" id="inputInitiative" class="form-control" placeholder="Initiative" style="width: 20%; margin: auto;">
	      <button class="btn btn-danger" name="action" type="submit" style="width: 20%">Submit</button>
    	</form>
			<br>
			<p>
			<a href='/Main/index' style="text-decoration: none; color: black;">Back To Main</a> |
			<a href="/Main/logout" style="text-decoration: none; color: black;">Log Out</a> | 
			<a href="/Roster/index" style="text-decoration: none; color: black;">Back to Roster</a>
			</p>
    	</div>
		
		<!--<form class="" action="/Roster/create" method="post">
		  <h3>Create Player</h3>
	      
	      <label for="inputName" class="">Name</label>
	      <input name="name" id="inputName" class="">
	      <br>
	      <label for="inputAC" class="">Armor Class</label>
	      <input name="AC" id="inputAC" class="">
	      <br>
	      <label for="inputPerception" class="">Passive Perception</label>
	      <input name="perception" id="inputPerception" class="">
	      <br>
	      <label for="inputInitiative" class="">Initiative</label>
	      <input name="initiative" id="inputInitiative" class="">
	      
	      <button class="btn btn-danger" name="action" type="submit">Submit</button>
    	</form> -->


	</body>

</html>