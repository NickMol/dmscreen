<?php
class EncounterController extends Controller{

	public function index(){
		$this->view('Encounter/index');
	}

	public function generate(){
		$cr = $_POST['cr'];
		$difficulty = $_POST['difficulty'];
		$terrain = $_POST['terrain'];
		
		//generate it
		$monsters = $this->model('Monster')->getMonsters_info($cr, $terrain);

		echo json_encode($monsters);
	}

 }
?>