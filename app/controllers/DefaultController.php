<?php
class DefaultController extends Controller{

	public function index(){
		$this->view('Default/login');
	}


	public function login(){
		try{
			if(isset($_POST['action'])){
				
				$email = $_POST['email'];
				$password = $_POST['password'];

				$user = $this->model('Login');
				$users = $user->logingIn($email);
				if(isset($users[0])){
					$user = $users[0];
					if(password_verify($password, $user->password)){
						$_SESSION['email'] = $email;
						$_SESSION['user_id'] = $user->id;
						header('location:/Game/index');//GO TO OTHER PLACE
					}else{//password is bad
						$this->view('Default/login',['error'=>'Invalid email or password.']);
					}
				}else{//no user
					$this->view('Default/login',['error'=>'Invalid email or password.']);
				}
			}else{//no post
				$this->view('Default/login');
			}
		}
		catch (Exception $e){
			$this->view('Default/login');
		}
	}
	
	
	public function signup(){
		if(isset($_POST['action'])){
			$user = $this->model('Login');
			$user->email = $_POST['email'];
			$user->password = password_hash($_POST['password'],PASSWORD_DEFAULT);
			try{
				$user->insert();
				header('location:/Default/login');
			}catch(Exception $e){
				$this->view('Default/signup', ['errormessage' => 'The email you have entered is already in the database.']);
			}
		}else{
			$this->view('Default/signup');
		}
	}
	
	public function logout(){
		// Détruit toutes les variables de session
		$_SESSION = array();

		// Si vous voulez détruire complètement la session, effacez également
		// le cookie de session.
		// Note : cela détruira la session et pas seulement les données de session !
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		// Finalement, on détruit la session.
		session_destroy();
		header('location:/Default/login');
	}

}
?>