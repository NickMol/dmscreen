<?php
class NPC extends Model{
	var $id;
	var $game_id;
	var $npc_name;
	var $npc_race;

	public function __construct(){
		parent::__construct();
	}

	public function insert(){
		$sql = "INSERT INTO Player_npc (game_id, npc_name, npc_race) VALUES (:game_id, :npc_name, :npc_race)";
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['game_id'=>$this->game_id,'npc_name'=>$this->npc_name,'npc_race'=>$this->npc_race]);
	}

	public function getNPCs(){
		$sql = "SELECT * FROM Player_npc ORDER BY npc_name ASC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "NPC");
		return $stmt->fetchAll();
	}

	public function find($id){
		$sql = "SELECT * FROM Player_npc WHERE id =:id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['id'=>$id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "NPC");
		return $stmt->fetch();
	}

	public function delete($game_id, $name, $race){
		$sql = "DELETE FROM Player_npc WHERE game_id = :game_id AND npc_name = :npc_name AND npc_race = :npc_race";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['game_id'=>$game_id, 'npc_name'=>$name, 'npc_race'=>$race]);
	}

	public function edit($id, $name, $race){
		echo " in model: " . $name;
		$sql = 'UPDATE player_npc SET npc_name = :name, npc_race = :race WHERE id = :id';
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['id'=>$id,'name'=>$name, 'race'=>$race]);
	}
}
?>