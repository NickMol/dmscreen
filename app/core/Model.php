<?php
class Model extends ModelCaller{
	static $_connection;
	//we do database stuff here
	public function __construct()
    {
		//database parameters
		$server = 'localhost';
		$DBName = 'dmscreen';
		$user = 'root';
		$pass = '';
	
        self::$_connection = new PDO("mysql:host=$server;dbname=$DBName;charset=utf8", $user, $pass);
        self::$_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
?>