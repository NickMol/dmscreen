<?php
class MainController extends Controller{

	public function index(){
		$notes = $this->model('Main')->getNotes();
		$this->view('Main/index', ['notes' => $notes]);
	}

	public function create(){
		if(isset($_POST['action'])){
			$note = $this->model('Main');
			//set values to model vars 
			$note->game_id = $_SESSION['game_id'];
			$note->note_name = $_POST['name'];
			$note->note_text = $_POST['text'];
			try{
				$note->insert();
				header('location:/Main/index');
			}catch(Exception $e){
				echo $e;
				$this->view('Main/create', ['errormessage' => 'Something went wrong']);
			}
		}else{
			$this->view('Main/create');
		}
	}

	public function remove($id){
		try{
			$note = $this->model('Main')->find($id);
			$model = $this->model('Main');
			$model->delete($note->game_id, $note->note_id);
			$notes = $model->getNotes();
			$this->view('Main/index', ['notes' => $notes]);
		}catch (Exception $e){
			echo "in catch  	$e";
			$notes = $this->model('Main')->getNotes();
			$this->view('Main/index', ['notes' => $notes]);
		}
	}

	public function edit($id){
		$note = $this->model('Main')->find($id);
		if(isset($_POST['action'])){
			try{
				$model = $this->model('Main');
				$model->edit($id, $_POST['text']);
				$this->view('Main/index', ['notes' => $model->getNotes()]);
			}catch(Exception $e){
				$notes = $this->model('Main')->getNotes();
				$this->view('Main/index', ['notes' => $notes]);
			}
		}else
			$this->view('Main/edit',['note'=>$note]);
	}



}
?>