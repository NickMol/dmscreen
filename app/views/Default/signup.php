<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/dice.png">

    <title>DmScreen - Register</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center" style="background-image: linear-gradient(rgb(255, 128, 128) , rgb(255, 204, 204));">
    <form class="form-signin" action="/Default/signup" method="post">
	  <h1>Register for: DMScreen</h1>
      <img class="mb-4" src="../images/dice.png" alt="" width="144" height="144">
      <h1 class="h3 mb-3 font-weight-normal">Create an Account</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="" style="margin-bottom:-1px; border-bottom-left-radius:0px; border-bottom-right-radius:0px;">
	  <label for="confirmPassword" class="sr-only">Confirm Password</label>
      <input type="password" name="confirmPassword" id="confirmPassword" class="form-control" placeholder="Confirm Password" required="" style="border-top-left-radius:0px; border-top-right-radius:0px;">
      <button class="btn btn-lg btn-danger btn-block" type="submit" name="action">Register</button>
	  <br>
	  <a href="/Default/index">Wait! I already have an Account!</a>
      <p class="mt-5 mb-3 text-muted"><a href="www.igloolabs.ca" style="color:grey; text-decoration:none;">Created By Nicholas and Steve</a></p>
    </form>
  

</body></html>