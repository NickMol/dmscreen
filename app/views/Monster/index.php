<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../images/dice.png">

    <title>DmScreen - Monster Library</title>
	
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Bootstrap core JS -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
	<!-- My CSS and JS -->
	<link href="../../css/mynicecss.css" rel="stylesheet">
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	
	<script>
		function roll4(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 4) + 1);
		}
		function roll6(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 6) + 1);
		}
		function roll8(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 8) + 1);
		}
		function roll10(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 10) + 1);
		}
		function roll12(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 12) + 1);
		}
		function roll20(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 20) + 1);
		}
		function roll100(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 100) + 1);
		}
		function dodiceformula(){
			var totalMultiplier = parseInt(document.getElementById("totalMultiplier").value);
			var numberOfDice = parseInt(document.getElementById("numberOfDice").value);
			var diceSize = parseInt(document.getElementById("diceSize").value);
			var modifier = parseInt(document.getElementById("modifier").value);
			var totalModifier = parseInt(document.getElementById("totalModifier").value);
			
			if(isNaN(totalModifier) || isNaN(numberOfDice) || isNaN(diceSize) || isNaN(modifier) || isNaN(totalModifier)){
				return;
			}
			var result = 0;
			for(var x = 0; x != totalMultiplier; x++){
				console.log("HERE!");
				for(var i = 0; i != numberOfDice; i++){
					result += (Math.floor((Math.random() * diceSize) + 1)) + modifier;
				}
			}
			result += totalModifier;
			document.getElementById("result").innerHTML = "Result: " + result;
		}

		function favorite(id){
			var image = document.getElementsByClassName("favorite")[id - 1];
			if(image.src ===  "http://localhost/images/notfavorite.png"){
				image.src = "../images/favorite.png";
				
			}else if(image.src ===  "http://localhost/images/favorite.png"){
				image.src = "../images/notfavorite.png";
			}
		}

		function viewMore(id){
			event.preventDefault();
			console.log(id);
			$.ajax({
		        url: '/Monster/viewMore',
		        type: 'POST',
		        dataType: "json",
		        data: {
		            id: id
		        }
		    }).done(function(data){
		            var json = JSON.parse(JSON.stringify(data));
		            if(json.length == 0)
		            	return;

		            //RESET FIELDS:
		            	document.getElementById("monstername").innerHTML = "";
		            	document.getElementById("sizetypealign").innerHTML = "";
		            	document.getElementById("statslist").innerHTML = "";
		            	document.getElementById("skills").innerHTML = "";
		            	document.getElementById("language").innerHTML = "";
		            	document.getElementById("challenge").innerHTML = "";
		            	document.getElementById("locations").innerHTML = "";
		            	document.getElementById("ability").innerHTML = "";
		            	document.getElementById("attack").innerHTML = "";

		            document.getElementById("monstername").innerHTML = json[0].name;
		            document.getElementById("sizetypealign").innerHTML = json[0].size + " " + json[0].type + ", " + json[0].alignment;
		            document.getElementById("statslist").innerHTML = "<table style='width: 100%; text-align: center;'><tr><td>STR</td><td>DEX</td><td>CON</td><td>INT</td><td>WIS</td><td>CHA</td></tr><tr><td>" + json[0].str_score + "</td><td>" + json[0].dex_score + "</td><td>" + json[0].con_score + "</td><td>" + json[0].int_score + "</td><td>" + json[0].wis_score + "</td><td>" + json[0].cha_score + "</td></tr></table>";
		            document.getElementById("skills").innerHTML = "<b>Skills: </b>" + json[0].skills;
		            document.getElementById("senses").innerHTML = "<b>Senses: </b>" + json[0].senses;
		            document.getElementById("language").innerHTML = "<b>Language: </b>" + json[0].language;
		            document.getElementById("challenge").innerHTML = "<b>Challenge Rating: </b>" + json[0].challenge_rating;
		            document.getElementById("locations").innerHTML = "<b>Locations: </b>" + json[0].location;
		            if(json[0].ability_name != ""){
		            	document.getElementById("ability").innerHTML = "<b>" + json[0].ability_name + "</b>. " + json[0].ability_description + "<br>";
		            }

		            if(json[0].action_name != ""){
		            	document.getElementById("attack").innerHTML = "<b>" + json[0].action_name + "</b>. " + json[0].action_description + "<br>";
		            }

		            var lastLocation = json[0].location;
		            var lastAbility = json[0].ability_name;
		            var lastAttack = json[0].action_name;

		            for(var i = 1; i < json.length; i++){
		            	if(!(lastLocation.includes(json[i].location)))
		            		document.getElementById("locations").innerHTML += ", " + json[i].location;
		            	if(!(lastAbility.includes(json[i].ability_name)))
		            		document.getElementById("ability").innerHTML += "<br><b>" + json[i].ability_name + "</b>. " + json[i].ability_description + "<br>";
		            	if(!(lastAttack.includes(json[i].action_name)))
		            		document.getElementById("attack").innerHTML += "<br><b>" + json[i].action_name + "</b>. " + json[i].action_description + "<br>";

		            	lastLocation = document.getElementById("locations").innerHTML
						lastAbility = document.getElementById("ability").innerHTML
						lastAttack = document.getElementById("attack").innerHTML
		            }

 					});	
		}	
	</script>
	
	</head>
	<body onload="viewMore(1)">
		<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-danger" id="topnav" style="border-bottom: 3px solid black;">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="/Main/index"><i class="far fa-sticky-note"></i> Notes</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/Roster/index"><i class="fas fa-list"></i> Player List</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="/Monster/index"><i class="fas fa-book"></i> Monster Guide</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/Encounter/index"><i class="fas fa-pastafarianism"></i> Encounter Generator</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/NPC/index"><i class="fas fa-user"></i> NPC Generator</a>
			</li>
		</ul>
		<ul class="navbar-nav mr-auto" style="margin-right: 20px!important;">
			<li class="nav-item">
				<a class="nav-link" href="/Account/index" style="float: right">Edit Account</a> <!--call new page directly -->
			</li>		
			<li class="nav-item">
				<a class="nav-link" href="/Default/logout">Log Out</a>
			</li>
		</ul>
			<h3 style="margin-right:20px;">DMScreen</h3>
			<img src="../../images/dice.png" alt="" width="60" height="60">
		</nav>
		<div id="mainbody">
			<h3>Monster Manual </h3>
			<br>

			<?php
				if($data['mon']){
					$monsters = $data['monsters']; 
				}
				if($data['fav']){
					$favorites = $data['favorites'];
				}
			?>

			<!-- Form for search -->
			<div style="width: 30%; margin-left: 20px;">
			<form class="form-group" action="/Monster/search" method="post">
			<div class="row">
			<div class="col" style="margin: 0px; padding-right: 0px;">
				<input class="form-control" name="nameSearched" placeholder="Search for a monster..." style="margin-right: 0px; padding-right: 0px;"/>
			</div>	
			<div class="col">
				<button class="btn btn-danger" name="action" type="submit" style="margin-left: 0px;">Search</button>
			</div>
			</div>
			</form>
				<a style="margin-left: 40px"class="text-muted" href="/Monster/index">View All Monsters</a> |
				<a class="text-muted" href="/Monster/viewFavorites">View Favorites</a>
			</div>

			<div style="width: 45%; height: 600px; overflow: auto; margin-left: 60px; float: left;">
			<table class="table table-striped table-danger table-bordered table-hover" style="text-align: center; width: 100%; white-space: nowrap;">
				<thead class="thead-dark">
					<tr><th>Name</th><th>Type | <a href="/Monster/order/asc" style="color: white; text-decoration: none;">^</a><a href="/Monster/order/desc" style="color: white; text-decoration: none;">v</a></th><th>View More</th><th>Favorite</th></tr>
				</thead>

				<?php 
				if($data['mon']){
					foreach($monsters as $monster){
							echo "<tr><td>{$monster->name}</td>
							<td>{$monster->type}</td>
							<td><button class='btn btn-danger' style='width: 90%' onClick='javascript:viewMore({$monster->id})'>More</button></td>
							<td><a href='/Monster/favorite/{$monster->id}'><img class='favorite' onclick='favorite({$monster->id})' src='../../images/".(is_null($monster->monster_id)?"not":"")."favorite.png' alt='' width='30' height='30' style='z-index: 9999'></a></td></tr>";
					}
				}
				else if($data['fav']){
					foreach($favorites as $monster){
						if($monster->game_id == $_SESSION['game_id']){
							echo "<tr><td>{$monster->name}</td>
							<td>{$monster->type}</td>
							<td><button class='btn btn-danger' style='width: 90%' onClick='javascript:viewMore({$monster->id})'>More</button></td>
							<td><a href='/Monster/favorite/" . $monster->id ."'><img class='favorite' onclick='favorite(" . $monster->id .")' src='../../images/favorite.png' alt='' width='30' height='30' style='z-index: 9999'></a></td></tr>";
						}
					}
				}
			?>			
			</table>
			</div>
			<div style="float: left; border: 3px solid black; width: 45%; height: 600px; overflow: auto; margin-left: 50px;">
				<h3 id="monstername"></h3>
				<p id="sizetypealign" style="border-bottom: 3px solid black;"></p>
				<p id="statslist" style="border-bottom: 3px solid black;"></p>
				<p id="skills" style="margin-bottom: 0px;"></p>
				<p id="senses" style="margin-bottom: 0px;"></p>
				<p id="language" style="margin-bottom: 0px;"></p>
				<p id="challenge" style="margin-bottom: 0px;"></p>
				<p id="locations" style="margin-bottom: 0px; border-bottom: 3px solid black;"></p>
				<br>
				<h5><b>Abilities</b></h5><br>
				<p id="ability" style="margin-bottom: 0px;"></p>
				<br>
				<h5><b>Actions</b></h5><br>
				<p id="attack" style="margin-bottom: 0px;"></p>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom" id="bottomnav">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll4()">
					<a class="nav-link inactive" onclick="roll4()">D4</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll6()">
					<a class="nav-link inactive" onclick="roll6()">D6</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll8()">
					<a class="nav-link inactive" onclick="roll8()">D8</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll10()">
					<a class="nav-link inactive" onclick="roll10()">D10</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll12()">
					<a class="nav-link inactive" onclick="roll12()">D12</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll20()">
					<a class="nav-link inactive" onclick="roll20()">D20</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll100()">
					<a class="nav-link inactive" onclick="roll100()">D100</a>
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-left:10px; margin-top:20px;">Dice Roller Formula: </a>
				</li>
				<li>
					<input type="text" class="form-control" id="totalMultiplier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">*(</a>
				</li>
				<li>
					<input type="text" class="form-control" id="numberOfDice" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">d</a>
				</li>
				<li>
					<input type="text" class="form-control" id="diceSize" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">+</a>
				</li>
				<li>
					<input type="text" class="form-control" id="modifier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">) + </a>
				</li>
				<li>
					<input type="text" class="form-control" id="totalModifier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<button class="btn btn-danger" style="margin-top:20px; margin-left:5px;" onclick="dodiceformula()">Roll!</button>
				</li>
			</ul>
			<a class="nav-link inactive" style="color:white; font-size:40px; margin-right:50px;" id="result">Result: XX</a>
		</nav>
	</body>
</html>