<?php
class Login extends Model{
	var $email;
	var $password;

	public function __construct(){
		parent::__construct();
	}

	public function logingIn($email){
		$sql = "SELECT * FROM Login WHERE email = :email;";
		$stmt = self::$_connection->prepare($sql);
		$stmt->execute(['email'=>$email]);

		$stmt->setFetchMode(PDO::FETCH_CLASS, "Login");
		return $stmt->fetchAll();
	}

	//assume all parameters are set
	public function insert(){
		$sql = "INSERT INTO Login (email, password) VALUES (:email, :password)";
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['email'=>$this->email,'password'=>$this->password]);
	}

	public function getAll(){
		$sql = "SELECT * FROM Login";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Login");
		return $stmt->fetchAll();
	}

	public function find($id){
		$sql = "SELECT * FROM Login WHERE id =:id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['id'=>$id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Login");
		return $stmt->fetch();
	}

	public function edit(){
		$sql = 'UPDATE Login SET password = :password WHERE id = :id';
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['id'=>$this->id,'password'=>$this->password]);
	}



	
}
?>