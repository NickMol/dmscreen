<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../images/dice.png">

    <title>DmScreen - Monster Library</title>
	
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Bootstrap core JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
	<!-- My CSS and JS -->
	<link href="../../css/mynicecss.css" rel="stylesheet">
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	
	<script>
		function roll4(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 4) + 1);
		}
		function roll6(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 6) + 1);
		}
		function roll8(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 8) + 1);
		}
		function roll10(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 10) + 1);
		}
		function roll12(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 12) + 1);
		}
		function roll20(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 20) + 1);
		}
		function roll100(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 100) + 1);
		}
		function dodiceformula(){
			var totalMultiplier = parseInt(document.getElementById("totalMultiplier").value);
			var numberOfDice = parseInt(document.getElementById("numberOfDice").value);
			var diceSize = parseInt(document.getElementById("diceSize").value);
			var modifier = parseInt(document.getElementById("modifier").value);
			var totalModifier = parseInt(document.getElementById("totalModifier").value);
			
			if(isNaN(totalModifier) || isNaN(numberOfDice) || isNaN(diceSize) || isNaN(modifier) || isNaN(totalModifier)){
				return;
			}
			var result = 0;
			for(var x = 0; x != totalMultiplier; x++){
				console.log("HERE!");
				for(var i = 0; i != numberOfDice; i++){
					result += (Math.floor((Math.random() * diceSize) + 1)) + modifier;
				}
			}
			result += totalModifier;
			document.getElementById("result").innerHTML = "Result: " + result;
		}
	</script>
	
	</head>
	<body>
		<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-danger" id="topnav" style="border-bottom: 3px solid black;">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="/Main/index"><i class="far fa-sticky-note"></i> Notes</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="/Roster/index"><i class="fas fa-list"></i> Player List</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/Monster/index"><i class="fas fa-book"></i> Monster Guide</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/Encounter/index"><i class="fas fa-pastafarianism"></i> Encounter Generator</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/NPC/index"><i class="fas fa-user"></i> NPC Generator</a>
			</li>
		</ul>
		<ul class="navbar-nav mr-auto" style="margin-right: 20px!important;">
			<li class="nav-item">
				<a class="nav-link" href="/Account/index" style="float: right">Edit Account</a> <!--call new page directly -->
			</li>		
			<li class="nav-item">
				<a class="nav-link" href="/Default/logout">Log Out</a>
			</li>
		</ul>
			<h3 style="margin-right:20px;">DMScreen</h3>
			<img src="../../images/dice.png" alt="" width="60" height="60">
		</nav>
		<div id="mainbody" style="margin-bottom: 100px">
			<h3>Player Roster: </h3>
			<br>
			<a href="/Roster/create" class="text-muted">Add new player or monster to your roster</a> |
			<a href="/Roster/order/0" class="text-muted">Sort by Initiative</a> 
		<div style="width: 100%; height: 650px; overflow: auto; float: left;">
			<table class="table table-striped table-danger table-bordered table-hover" style="text-align: center; width: 100%; white-space: nowrap;">
			<thead class="thead-dark"><tr> <th>Player Name</th> <th>Armor Class</th> <th>Passive Perception</th> <th>Initiative (Click to Change)</th><th>Delete</th></tr></thead>
			
			<?php 
			foreach($data['players'] as $player){
				if($player->game_id === $_SESSION['game_id']){
					echo "<tr><td>{$player->name}</td>
					<td>{$player->armor_class}</td>
					<td>{$player->passive_perception}</td>
					<td><a href='/Roster/edit/{$player->id}' class=\"text-muted\" style=\"text-color: black; text-decoration: none;\">{$player->initiative}</a> <a href='/Roster/addToInitiative/{$player->id}'><i class='fas fa-plus' style='color: rgb(101, 102, 99)'></i></a> <a href='/Roster/subFromInitiative/{$player->id}'><i class='fas fa-minus' style='color: rgb(101, 102, 99)'></i></a></td>
					<td><button class=\"btn btn-danger\" onclick=\"location.href='/Roster/remove/" . $player->id . "'\" type=\"button\" style=\"width: 90%\">Delete</button></td></tr>";
				}
			}
			?>
			
		</table>
	</div>
	</div>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom" id="bottomnav">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll4()">
					<a class="nav-link inactive" onclick="roll4()">D4</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll6()">
					<a class="nav-link inactive" onclick="roll6()">D6</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll8()">
					<a class="nav-link inactive" onclick="roll8()">D8</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll10()">
					<a class="nav-link inactive" onclick="roll10()">D10</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll12()">
					<a class="nav-link inactive" onclick="roll12()">D12</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll20()">
					<a class="nav-link inactive" onclick="roll20()">D20</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../../images/dice.png" alt="" width="40" height="40" onclick="roll100()">
					<a class="nav-link inactive" onclick="roll100()">D100</a>
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-left:10px; margin-top:20px;">Dice Roller Formula: </a>
				</li>
				<li>
					<input type="text" class="form-control" id="totalMultiplier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">*(</a>
				</li>
				<li>
					<input type="text" class="form-control" id="numberOfDice" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">d</a>
				</li>
				<li>
					<input type="text" class="form-control" id="diceSize" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">+</a>
				</li>
				<li>
					<input type="text" class="form-control" id="modifier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">) + </a>
				</li>
				<li>
					<input type="text" class="form-control" id="totalModifier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<button class="btn btn-danger" style="margin-top:20px; margin-left:5px;" onclick="dodiceformula()">Roll!</button>
				</li>
			</ul>
			<a class="nav-link inactive" style="color:white; font-size:40px; margin-right:50px;" id="result">Result: XX</a>
		</nav>
	</body>
</html>