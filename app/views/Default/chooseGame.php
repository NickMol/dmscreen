<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/dice.png">

    <title>DmScreen - Choose Your Game</title>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center" style="background-image: linear-gradient(rgb(255, 128, 128) , rgb(255, 204, 204));">
    <form class="form-signin" action="/Default/login" method="post">
	  <h1>Welcome to: DMScreen</h1>
      <img class="mb-4" src="../images/dice.png" alt="" width="144" height="144">
      <h1 class="h3 mb-3 font-weight-normal">Pick Your Game</h1>
	  <div class="dropdown">
		<button class="btn btn-lg btn-danger btn-block dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		  Pick Your Game
		  </button>
		  <div class="dropdown-menu" aria-labelledby="dropdownMenu2" style="width: 100%;">
			<button class="dropdown-item" type="button">Game 1</button>
			<button class="dropdown-item" type="button">Game 2</button>
			<button class="dropdown-item" type="button">Game 3</button>
		</div>
	  </div>
	  <br>
	  <br>
	  <h1 class="h3 mb-3 font-weight-normal">Or Create A New One</h1>
      <label for="inputEmail" class="sr-only">Game Name</label>
      <input type="game" name="game" id="inputGame" class="form-control" placeholder="Game Name" required="" autofocus="" style="margin-bottom: -1px; border-bottom-left-radius: 0; border-bottom-right-radius: 0;">
      <button class="btn btn-lg btn-danger btn-block" type="submit" name="action" style="margin-bottom: 10px; border-top-left-radius: 0; border-top-right-radius: 0;">Create Game</button>
	  <br>
      <p class="mt-5 mb-3 text-muted"><a href="www.igloolabs.ca" style="color:grey; text-decoration:none;">Created By Nicholas and Steve</a></p>
    </form>
</body></html>