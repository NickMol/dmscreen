<!--<?php $user = ($data['user']); ?>	
		<p>
		<a href='/Main/index'>Back To Main</a> |
		<a href="/Main/logout">Log Out</a> 
		</p>
		<form class="" action="/Account/edit" method="post">
		  <h3>Edit Account</h3>
	      
	      <label for="inputEmail" class="">Email Address</label>
	      <input type="email" name="email" id="inputEmail" class="" 
	      	value=<?php echo $user->email; ?> readonly>
	      <br>
	      <label for="inputPassword" class="">Password</label>
	      <input type="password" name="password" id="inputPassword" class=""
	       value="" >
	      <p>"DO NOT TOUCH TO KEEP SAME PASSWORD" in red or something</p>
	      <button class="btn btn-danger" name="action" type="submit">Submit</button>
    	</form>-->
		
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../images/dice.png">

    <title>DmScreen - Modify Account</title>
 
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center" style="background-image: linear-gradient(rgb(255, 128, 128) , rgb(255, 204, 204));">
    <form class="form-signin form-group" action="/Default/login" method="post">
	  <h1>DMScreen</h1>
      <img class="mb-4" src="../images/dice.png" alt="" width="144" height="144">
      <h1 class="h3 mb-3 font-weight-normal">Change Your Password</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" name="email" id="inputEmail" class="form-control" value=<?php echo $user->email; ?> readonly>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" value="" >
      <button class="btn btn-lg btn-danger btn-block" type="submit" name="action">Change Password</button>
	  <br>
	  <a href="/Default/signup">No Account? Sign up here!</a>
      <p class="mt-5 mb-3 text-muted"><a href="www.igloolabs.ca" style="color:grey; text-decoration:none;">Created By Nicholas and Steve</a></p>
    </form>
  

</body></html>