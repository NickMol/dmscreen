<?php
class NPCController extends Controller{

	 public function index(){
        $response = file_get_contents('https://uinames.com/api/?region=United%20States');
                $response = json_decode($response);
                $races = array("Elf", "Dwarf", "Human", "Half-Elf", "Half-Orc", "Orc");
                $race = $races[rand(0, 5)];
                $npcs = $this->model('NPC')->getNPCs();
                $this->view("NPC/index", ['name' => $response, 'race' => $race, 'npcs' => $npcs]);
     }

    public function generate(){
                $response = file_get_contents('https://uinames.com/api/?region=United%20States');
                $response = json_decode($response);
                $races = array("Elf", "Dwarf", "Human", "Half-Elf", "Half-Orc", "Orc");
                $race = $races[rand(0, 5)];
				$npcs = $this->model('NPC')->getNPCs();
                $this->view("NPC/index", ['name' => $response, 'race' => $race, 'npcs' => $npcs]);
    }

    public function save(){
        if(isset($_POST['action'])){
            $npc = $this->model('NPC');
            //set values to model vars 
            $npc->game_id = $_SESSION['game_id'];
            $npc->npc_name = $_POST['first'] . " " . $_POST['last'];
            $npc->npc_race = $_POST['race'];
            try{
                $npc->insert();
                header('location:/NPC/index');
            }catch(Exception $e){
                $this->view('NPC/index', ['errormessage' => 'Something went wrong']);
            }
        }else{
            $this->view('NPC/index');
        }
    }

    public function remove($id){
        try{
            $npc = $this->model('NPC')->find($id);
            $model = $this->model('NPC');
            $model->delete($npc->game_id, $npc->npc_name, $npc->npc_race);
            $npcs = $this->model('NPC')->getNPCs();
            $this->index();
        }catch (Exception $e){
            echo $e;
            $this->index();
        }
    }

    public function edit($id){
        $npc = $this->model('NPC')->find($id);
        if(isset($_POST['action'])){
            try{
                echo $_POST['name'];
                $model = $this->model('NPC');
                $model->edit($id, $_POST['name'], $_POST['race']);
                $this->index();
            }catch(Exception $e){
                $this->index();
            }
        }else
            $this->view('NPC/edit',['npc'=>$npc]);
    }
 }
 ?>