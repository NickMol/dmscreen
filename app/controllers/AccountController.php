<?php
class AccountController extends Controller{

	public function index(){
		$user = $this->model('Login')->find($_SESSION['user_id']);
		$this->view('Account/index', ['user' => $user]);

	}

	public function edit(){
		try{
			$user = $this->model('Login')->find($_SESSION['user_id']);
			if(isset($_POST['action'])){
				if("" == $_POST['password']){
					$user->password = $_POST['password'];
				}else{
					$user->password = password_hash($_POST['password'],PASSWORD_DEFAULT);
				}
				$user->edit();
				header('location: /Account/index/' . $user->id);			
			}
			else{
				$this->view('Account/index', ['user' => $user]);
			}
		}catch (Exception $e) {
			echo $e;
		}
	}

	
	
}
?>