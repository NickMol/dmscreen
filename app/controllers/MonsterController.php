<?php
class MonsterController extends Controller{

	public function index(){
		$monsters = $this->model('Monster')->getMonsters();
		$this->view('Monster/index', ['monsters' => $monsters, 'mon' => true, 'fav' => false]);
	}

	public function viewFavorites(){
		$favorites = $this->model('Monster')->getFavorites();
		$this->view('Monster/index', ['favorites' => $favorites,  'mon' => false, 'fav' => true]);
	}

	public function viewMore(){
		$id = $_POST['id'];
		try{
			$monster = $this->model('Monster')->getMonsterStats($id);
			echo json_encode($monster);
			/*$monsters = $this->model('Monster')->getMonsters();
			$this->view('Monster/index', ['monsters' => $monsters]);*/
		}catch (Exception $e){
			echo $e;
		}
	}

	public function search(){
		if(isset($_POST['action'])){
			//echo $_POST['nameSearched'];
			$monsters = $this->model('Monster')->search($_POST['nameSearched']);
			$this->view('Monster/index', ['monsters' => $monsters, 'mon' => true, 'fav' => false]);
		}
		else{
			$monsters = $this->model('Monster')->getMonsters();
			$this->view('Monster/index', ['monsters' => $monsters, 'mon' => true, 'fav' => false]);
		}
	}

	public function order($order){
		if($order == 'asc'){
			$model = $this->model('Monster');
			$monsters = $model->orderAsc();
			$this->view('Monster/index', ['monsters' => $monsters]);
		}
		else if($order == 'desc'){
			$model = $this->model('Monster');
			$monsters = $model->orderDesc();
			$this->view('Monster/index', ['monsters' => $monsters]); 
		}
		else
			$this->view('Monster/index', ['monsters' => $monsters]);
	}

	public function favorite($id){
		$isin = $this->model('Player_Monsters')->isInTable($id, $_SESSION['game_id']);
		$pm = $this->model('Player_Monsters');
		if(!$isin){
			//echo "!isin ". $id;
			$pm->monster_id = $id;
			$pm->game_id = $_SESSION['game_id'];
			$pm->insert();
		}
		else {
			$pm->delete($_SESSION['game_id'], $id);
		}
		$this->viewFavorites();
	}
	
}
?>