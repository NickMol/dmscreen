<?php
class Player_Monsters extends Model{
	var $monster_id;
	var $game_id;

	public function __construct(){
		parent::__construct();
	}

	public function insert(){
		//echo "in insert";
		$sql = "INSERT INTO Player_Monsters (monster_id, game_id) VALUES (:monster_id, :game_id)";
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['monster_id'=>$this->monster_id,'game_id'=>$this->game_id]);
	}

	public function delete($game_id, $id){
		$sql = "DELETE FROM Player_Monsters WHERE game_id = :game_id AND monster_id = :id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['game_id'=>$game_id, 'id'=>$id]);
	}

	public function getFavorites(){
		$sql = "SELECT * FROM Player_Monsters ORDER BY monster_id ASC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Player_Monsters");
		return $stmt->fetchAll();
	}

	public function isInTable($id, $game_id){
		$sql = "SELECT * FROM Player_Monsters WHERE monster_id = :id AND game_id = :game_id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['id'=>$id, 'game_id' => $game_id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Player_Monsters");
		return $stmt->fetch();
	}
}
?>