<?php
class RosterController extends Controller{

	public function index(){
		$players = $this->model('Roster')->getRoster();
		$this->view('Roster/index', ['players' => $players]);
	}

	public function create(){
		if(isset($_POST['action'])){
			$player = $this->model('Roster');
			//set values to model vars 
			$player->game_id = $_SESSION['game_id'];
			$player->name = $_POST['name'];
			$player->armor_class = $_POST['AC'];
			$player->passive_perception = $_POST['perception'];
			$player->initiative = $_POST['initiative'];
			try{
				$player->insert();
				header('location:/Roster/index');
			}catch(Exception $e){
				echo "in catch create" . $e;
				$this->view('Roster/create', ['errormessage' => 'Something went wrong']);
			}
		}else{
			$this->view('Roster/create');
		}
	}

	public function remove($id){
		try{
			$player = $this->model('Roster')->find($id);
			$model = $this->model('Roster');
			$model->delete($player->game_id, $player->name);
			$players = $model->getRoster();
			$this->view('Roster/index', ['players' => $players]);
		}catch (Exception $e){
			$this->index();
		}
	}

	public function edit($id){
		$player = $this->model('Roster')->find($id);
		if(isset($_POST['action'])){
			try{
				$model = $this->model('Roster');
				$model->editInitiative($id, $_POST['initiative']);
				$this->view('Roster/index', ['players' => $model->getRoster()]);
			}catch(Exception $e){

			}
		}else
			$this->view('Roster/edit',['player'=>$player]);
	}

	public function addToInitiative($id){
		$player = $this->model('Roster')->find($id);
		
			try{
				$model = $this->model('Roster');
				$model->editInitiative($id, $player->initiative + 1);
				$this->view('Roster/index', ['players' => $model->getRoster()]);
			}catch(Exception $e){

			}
	}

	public function subFromInitiative($id){
		$player = $this->model('Roster')->find($id);
		
			try{
				$model = $this->model('Roster');
				$model->editInitiative($id, $player->initiative - 1);
				$this->view('Roster/index', ['players' => $model->getRoster()]);
			}catch(Exception $e){

			}
	}

	public function order($num){
		$players;
		if($num == 0){
			$model = $this->model('Roster');
			$players = $model->orderDesc(); 
		}
		$this->view('Roster/index', ['players' => $players]);
	}
	
}
?>