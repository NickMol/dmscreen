<?php
class Main extends Model{
	var $note_id;
	var $game_id;
	var $note_name;
	var $note_text;
	var $note_date;

	public function __construct(){
		parent::__construct();
	}

	public function insert(){
		$sql = "INSERT INTO Notes (game_id, note_name, note_text, note_date) VALUES (:game_id, :note_name, :note_text, NOW())";
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['game_id' => $this->game_id, 'note_name' => $this->note_name, 'note_text' => $this->note_text]);
	}

	public function getNotes(){
		$sql = "SELECT * FROM Notes ORDER BY note_date DESC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Main");
		return $stmt->fetchAll();
	}

	public function find($note_id){
		$sql = "SELECT * FROM Notes WHERE note_id =:note_id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['note_id'=>$note_id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Main");
		return $stmt->fetch();
	}

	public function delete($game_id, $note_id){
		$sql = "DELETE FROM Notes WHERE game_id = :game_id AND note_id = :note_id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['game_id'=>$game_id, 'note_id'=>$note_id]);
	}

	


	public function edit($note_id, $note_text){
		$sql = 'UPDATE Notes SET note_text = :note_text WHERE note_id = :note_id';
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['note_id'=>$note_id,'note_text'=>$note_text]);
	}


	public function orderDesc(){
		$sql = "SELECT * FROM Notes ORDER BY initiative DESC";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Main");
		return $stmt->fetchAll();
	}
	
}
?>