<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/dice.png">

    <title>DmScreen - Login</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center" style="background-image: linear-gradient(rgb(255, 128, 128) , rgb(255, 204, 204));">
    <form class="form-signin" action="/Default/login" method="post">
	  <h1>Welcome to: DMScreen</h1>
      <img class="mb-4" src="../images/dice.png" alt="" width="144" height="144">
      <h1 class="h3 mb-3 font-weight-normal">Sign In</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
      <button class="btn btn-lg btn-danger btn-block" type="submit" name="action">Sign in</button>
	  <br>
	  <a href="/Default/signup">No Account? Sign up here!</a>
      <p class="mt-5 mb-3 text-muted"><a href="https://www.igloolabs.ca" style="color:grey; text-decoration:none;">Created By Nicholas and Steve</a></p>
    </form>
  

</body></html>