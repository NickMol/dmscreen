<?php
class Game extends Model{
	var $id;
	var $dm_id;
	var $game_name;
	var $last_accessed;

	public function __construct(){
		parent::__construct();
	}

	public function chooseGame($email){
		$sql = "SELECT * FROM Game WHERE email = :email;";
		$stmt = self::$_connection->prepare($sql);
		$stmt->execute(['email'=>$email]);

		$stmt->setFetchMode(PDO::FETCH_CLASS, "Game");
		return $stmt->fetchAll();
	}

	//assume all parameters are set
	public function insert(){
		$sql = "INSERT INTO Game (dm_id, game_name, last_accessed) VALUES (:dm_id, :game_name, :last_accessed)";
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['dm_id' => $this->dm_id, 'game_name' => $this->game_name, 'last_accessed' => $this->last_accessed]);
	}

	public function getGames(){
		$sql = "SELECT * FROM Game";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Game");
		return $stmt->fetchAll();
	}

	public function find($id){
		$sql = "SELECT * FROM Login WHERE id =:id";
        $stmt = self::$_connection->prepare($sql);
        $stmt->execute(['id'=>$id]);

        $stmt->setFetchMode(PDO::FETCH_CLASS, "Game");
		return $stmt->fetch();
	}

	public function edit(){
		$sql = 'UPDATE Login SET password = :password WHERE id = :id';
		$sth = self::$_connection->prepare($sql);
		$sth->execute(['id'=>$this->id,'password'=>$this->password]);
	}



	
}
?>