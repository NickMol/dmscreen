<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/dice.png">

    <title>DmScreen - Encounter Generator</title>
	
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Bootstrap core JS -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
	<!-- My CSS and JS -->
	<link href="../css/mynicecss.css" rel="stylesheet">
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	
	<script>
		function roll4(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 4) + 1);
		}
		function roll6(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 6) + 1);
		}
		function roll8(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 8) + 1);
		}
		function roll10(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 10) + 1);
		}
		function roll12(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 12) + 1);
		}
		function roll20(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 20) + 1);
		}
		function roll100(){
			document.getElementById("result").innerHTML = "Result: " + Math.floor((Math.random() * 100) + 1);
		}
		function dodiceformula(){
			var totalMultiplier = parseInt(document.getElementById("totalMultiplier").value);
			var numberOfDice = parseInt(document.getElementById("numberOfDice").value);
			var diceSize = parseInt(document.getElementById("diceSize").value);
			var modifier = parseInt(document.getElementById("modifier").value);
			var totalModifier = parseInt(document.getElementById("totalModifier").value);
			
			if(isNaN(totalModifier) || isNaN(numberOfDice) || isNaN(diceSize) || isNaN(modifier) || isNaN(totalModifier)){
				return;
			}
			var result = 0;
			for(var x = 0; x != totalMultiplier; x++){
				console.log("HERE!");
				for(var i = 0; i != numberOfDice; i++){
					result += (Math.floor((Math.random() * diceSize) + 1)) + modifier;
				}
			}
			result += totalModifier;
			document.getElementById("result").innerHTML = "Result: " + result;
		}
		
		function addPlayer(){
			event.preventDefault();
			var playerName = document.getElementById("playerName").value;
			var playerLevel = document.getElementById("playerLevel").value;
			
			document.getElementById("players").innerHTML += "<tr id='" + playerName + "'><td>" + playerName + "</td><td>" + playerLevel + "</td><td><button class='btn btn-danger' style='width: 50%;' onclick='removePlayer(" + playerName + ")'>Delete</button></td></tr>";
			
			document.getElementById("playerName").value = "";
			document.getElementById("playerLevel").value = "";
		}
		
		function removePlayer(player){
			event.preventDefault();
			player.outerHTML = "";
		}

		function generateEncounter(){
			event.preventDefault();
			
			var difficultymodifier;			
			if($('#difficulty option:selected').text() == "Trivial")
				difficultymodifier = -2;
			else if($('#difficulty option:selected').text() == "Easy")
				difficultymodifier = -1;
			else if($('#difficulty option:selected').text() == "Medium")
				difficultymodifier = 0;
			else if($('#difficulty option:selected').text() == "Hard")
				difficultymodifier = 1;
			else if($('#difficulty option:selected').text() == "Deadly")
				difficultymodifier = 2;
			var oTable = document.getElementById('players');
			var rowLength = oTable.rows.length;
			var levelArray = [];
 
			for (i = 0; i < rowLength; i++){
			   var oCells = oTable.rows.item(i).cells;
			   var cellLength = oCells.length;
			   for(var j = 0; j < cellLength; j++){
			   	  if(j == 1){
			   	  	levelArray.push(oCells.item(j).innerHTML);
			      }
			   }
			}


			var monsterArray = [];
		    var crArray = [];
		    var ammountArray = [];

			if(levelArray.length == 0){
				fillTable(monsterArray, crArray, ammountArray);
				return;
			}

			var sum = 0;
			for( var i = 0; i < levelArray.length; i++ ){
			    sum += parseInt(levelArray[i]);
			}

			var cr = Math.round(sum/levelArray.length) + difficultymodifier;
			if(cr < 0)
				cr = 0;

			$.ajax({
		        url: '/Encounter/generate',
		        type: 'POST',
		        dataType: "json",
		        data: {
		            cr: cr,
		            terrain: $('#location').val(),
		            difficulty: $('#difficulty').val()
		        }
		    }).done(function(data){
		            var json = $.parseJSON(JSON.stringify(data));
					$(json).each(function(i,val){
					    $.each(val,function(k,v){
					          if(k == "name"){
					          	monsterArray.push(v);
					          	ammountArray.push(1);
					          }
					          if(k == "challenge_rating")
					          	crArray.push(v)
					});
					});
				});

		    $.ajax({
		        url: '/Encounter/generate',
		        type: 'POST',
		        dataType: "json",
		        data: {
		            cr: Math.floor(cr/2),
		            terrain: $('#location').val(),
		            difficulty: $('#difficulty').val()
		        }
		    }).done(function(data){
		            var json = $.parseJSON(JSON.stringify(data));
					$(json).each(function(i,val){
					    $.each(val,function(k,v){
					          if(k == "name"){
					          	monsterArray.push(v);
					          	ammountArray.push(2);
					          }
					          if(k == "challenge_rating")
					          	crArray.push(v);
					});
					});
					fillTable(monsterArray, crArray, ammountArray);	
				});
		}

		function fillTable(monsterArray, crArray, ammountArray){
			var tablevalues = "";
			for(var i = 0; i != monsterArray.length; i++){
				tablevalues += "<tr><td>" + monsterArray[i] + "</td><td>" + crArray[i] + "</td><td>" + ammountArray[i] + "</td></tr>";
			}
			document.getElementById("monsters").innerHTML = tablevalues;
		}
	</script>
	
	</head>
	<body>
		<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-danger" id="topnav" style="border-bottom: 3px solid black;">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="/Main/index"><i class="far fa-sticky-note"></i> Notes</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/Roster/index"><i class="fas fa-list"></i> Player List</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/Monster/index"><i class="fas fa-book"></i> Monster Guide</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="/Encounter/index"><i class="fas fa-pastafarianism"></i> Encounter Generator</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/NPC/index"><i class="fas fa-user"></i> NPC Generator</a>
			</li>
		</ul>
		<ul class="navbar-nav mr-auto" style="margin-right: 20px!important;">
			<li class="nav-item">
				<a class="nav-link" href="/Account/index" style="float: right">Edit Account</a> <!--call new page directly -->
			</li>		
			<li class="nav-item">
				<a class="nav-link" href="/Default/logout">Log Out</a>
			</li>
		</ul>
			<h3 style="margin-right:20px;">DMScreen</h3>
			<img src="../../images/dice.png" alt="" width="60" height="60">
		</nav>
		<div id="mainbody">
			<h3>Encounter Generator: </h3>
			<br>
			<div style="width: 45%; height: 400px; overflow: auto; margin-left: 60px; float: left;">
			<table class="table table-striped table-danger table-bordered table-hover" style="text-align: center; width: 100%; white-space: nowrap;">
				<thead class="thead-dark">
					<tr><th>Character Name</th><th>Level</th><th>Delete</th></tr>
				</thead>
				<tbody  id="players">
				</tbody>
			</table>
			</div>
			<div style="width: 45%; height: 400px; overflow: auto; margin-left: 60px; float: left;">
			<table class="table table-striped table-danger table-bordered table-hover" style="text-align: center; width: 100%; white-space: nowrap;">
				<thead class="thead-dark">
					<tr><th>Monster</th><th>Challenge Rating</th><th>Amount</th></tr>
				</thead>
				<tbody  id="monsters">
				</tbody>				
			</table>
			</div>
			<div style="text-align: center; width: 45%; float: left; clear: both; margin-left: 60px;">
				<h3 style="text-align: left;">Add a Player: </h3>
				<form>
				  <div class="form-group">
					<input type="text" class="form-control" id="playerName" placeholder="Player Name">
				  </div>
				  <div class="form-group">
					<input type="number" class="form-control" id="playerLevel" placeholder="Character Level">
				  </div>
				  <button class="btn btn-danger" onclick="addPlayer()">Add Player</button>
				</form>
			</div>
			<div style="width: 45%; float: left; margin-left: 60px; margin-bottom: 100px;">
				<h6 class="text-muted">Each Row Is A Singe Encounter</h6>
				<h3 style="text-align: left;">Generate an Encounter: </h3>
				<form>
				  <div class="form-group">
					<label for="location">Encounter Location: </label>
					<select class="form-control" id="location">
					  <option>Arctic</option>
					  <option>Coast</option>
					  <option>Desert</option>
					  <option>Forest</option>
					  <option>Grassland</option>
					  <option>Hill</option>
					  <option>Mountain</option>
					  <option>Swamp</option>
					  <option>Underdark</option>
					  <option>Underwater</option>
					  <option>Urban</option>
					</select>
				  </div>
				  <div class="form-group">
					<label for="difficulty">Encounter Difficulty: </label>
					<select class="form-control" id="difficulty">
					  <option>Trivial</option>
					  <option>Easy</option>
					  <option>Medium</option>
					  <option>Hard</option>
					  <option>Deadly</option>
					</select>
				  </div>
				  <button type="submit" class="btn btn-danger" onclick="generateEncounter()">Generate Encounter</button>
				</form>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-bottom" id="bottomnav">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item" style="margin-right:20px;">
					<img src="../images/dice.png" alt="" width="40" height="40" onclick="roll4()">
					<a class="nav-link inactive" onclick="roll4()">D4</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../images/dice.png" alt="" width="40" height="40" onclick="roll6()">
					<a class="nav-link inactive" onclick="roll6()">D6</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../images/dice.png" alt="" width="40" height="40" onclick="roll8()">
					<a class="nav-link inactive" onclick="roll8()">D8</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../images/dice.png" alt="" width="40" height="40" onclick="roll10()">
					<a class="nav-link inactive" onclick="roll10()">D10</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../images/dice.png" alt="" width="40" height="40" onclick="roll12()">
					<a class="nav-link inactive" onclick="roll12()">D12</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../images/dice.png" alt="" width="40" height="40" onclick="roll20()">
					<a class="nav-link inactive" onclick="roll20()">D20</a>
				</li>
				<li class="nav-item" style="margin-right:20px;">
					<img src="../images/dice.png" alt="" width="40" height="40" onclick="roll100()">
					<a class="nav-link inactive" onclick="roll100()">D100</a>
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-left:10px; margin-top:20px;">Dice Roller Formula: </a>
				</li>
				<li>
					<input type="text" class="form-control" id="totalMultiplier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">*(</a>
				</li>
				<li>
					<input type="text" class="form-control" id="numberOfDice" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">d</a>
				</li>
				<li>
					<input type="text" class="form-control" id="diceSize" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">+</a>
				</li>
				<li>
					<input type="text" class="form-control" id="modifier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<a class="nav-link inactive" style="color:white; font-size:20px; margin-top:20px;">) + </a>
				</li>
				<li>
					<input type="text" class="form-control" id="totalModifier" placeholder="0" style="width:50px; margin-top:20px;" value="0">
				</li>
				<li>
					<button class="btn btn-danger" style="margin-top:20px; margin-left:5px;" onclick="dodiceformula()">Roll!</button>
				</li>
			</ul>
			<a class="nav-link inactive" style="color:white; font-size:40px; margin-right:50px;" id="result">Result: XX</a>
		</nav>
	</body>
</html>